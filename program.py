from turtle import update
import PySimpleGUI as gui
import matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.pyplot import axes

def update_figure(data):
    axes = fig.axes
    x = [i[0] for i in data]
    y = [int(i[1]) for i in data]
    axes[0].plot(x,y,'r-')
    fig_can_agg.draw()
    fig_can_agg.get_tk_widget().pack()
    


gui.theme('DarkTeal6')
table_contact = []
layout = [[gui.Table(headings=['Observation','Results'], values= table_contact, expand_x=True, hide_vertical_scroll=True, key= '-TABLE-')],
          [gui.Input(key='-INPUT-',expand_x=True),gui.Button('Submit',size=(6,1))],
          [gui.Canvas(key = '-CANVAS-')]]
window = gui.Window("Graph Maker", layout,finalize=True)
window.Maximize()
# mat plot lib
fig = matplotlib.figure.Figure(figsize = (30,5))
fig.add_subplot(111).plot([],[])
fig_can_agg = FigureCanvasTkAgg(fig,window['-CANVAS-'].TKCanvas)
fig_can_agg.draw()
fig_can_agg.get_tk_widget().pack()


while True:
    event, values = window.read()

    if event == gui.WIN_CLOSED:
        break
    
    if event ==  'Submit':
        new_value = values['-INPUT-']
        if new_value.isnumeric():
            table_contact.append([len(table_contact)+1,float(new_value)])
            window['-TABLE-'].update(table_contact)
            window['-INPUT-'].update('')
            update_figure(table_contact)
window.close()
